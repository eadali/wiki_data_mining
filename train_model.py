#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Sep  1 00:10:35 2019

@author: eadali
"""

import pandas
from sklearn.pipeline import Pipeline
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.naive_bayes import MultinomialNB
from sklearn import metrics
from sklearn.model_selection import train_test_split



# CONFIGS
# =============================================================================
datapath = 'pages.csv'

remove_words = list()
remove_words.extend(['machine','learning','algorithm'])
remove_words.extend(['cancer'])
remove_words.extend(['2019','film'])
remove_words.extend(['linux','game', 'games'])
remove_words.extend(['england', 'football', 'team', 'f.c.'])
# =============================================================================



# PREPROCESS DATA
# =============================================================================
# loads data from file
dataframe = pandas.read_csv(datapath)

# gets page text data
x_data = dataframe['text'].str.lower()

# removes overfitting words
for word in remove_words:
    x_data = x_data.str.replace(word, '')


# gets label id of page
y_data, _ = pandas.factorize(dataframe['category'])

# splits data to train and test
x_train, x_test, y_train, y_test = train_test_split(x_data, y_data, test_size=0.2, random_state=42)
# =============================================================================



# CREATE ML MODEL
# =============================================================================
classifier = Pipeline([('vectorizer', TfidfVectorizer()),
                       ('clf', MultinomialNB(alpha=0.01))])

classifier.fit(x_train, y_train)
# =============================================================================


# TRAIN ML MODEL
# =============================================================================
classifier.fit(x_train, y_train)
# =============================================================================



# TEST ML MODEL
# =============================================================================
y_pred = classifier.predict(x_test)
score = metrics.f1_score(y_test, y_pred, average='macro')
print('score..:', score)
# =============================================================================