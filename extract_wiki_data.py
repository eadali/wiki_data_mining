#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Aug 31 15:02:32 2019

@author: eadali
"""

import requests
from bs4 import BeautifulSoup
import pandas



# CONFIGS
# =============================================================================
datapath = 'pages.csv'
# =============================================================================



# CATEGORY URLS
# =============================================================================
category_urls = dict()
category_urls['ml_algorithms'] = 'https://www.wikizeroo.org/index.php?q=aHR0cHM6Ly9lbi53aWtpcGVkaWEub3JnL3dpa2kvQ2F0ZWdvcnk6TWFjaGluZV9sZWFybmluZ19hbGdvcml0aG1z'
category_urls['cancer'] = 'https://www.wikizeroo.org/index.php?q=aHR0cHM6Ly9lbi53aWtpcGVkaWEub3JnL3dpa2kvQ2F0ZWdvcnk6Q2FuY2Vy'
category_urls['2019_films'] = 'https://www.wikizeroo.org/index.php?q=aHR0cHM6Ly9lbi53aWtpcGVkaWEub3JnL3dpa2kvQ2F0ZWdvcnk6MjAxOV9maWxtcw'
category_urls['linux_games'] = 'https://www.wikizeroo.org/index.php?q=aHR0cHM6Ly9lbi53aWtpcGVkaWEub3JnL3dpa2kvQ2F0ZWdvcnk6TGludXhfZ2FtZXM'
category_urls['eng_team'] = 'https://www.wikizeroo.org/index.php?q=aHR0cHM6Ly9lbi53aWtpcGVkaWEub3JnL3dpa2kvQ2F0ZWdvcnk6Rm9vdGJhbGxfY2x1YnNfaW5fRW5nbGFuZA'
# =============================================================================



# GET INTRODUCTIONS OF PAGES
# =============================================================================
dataframe = pandas.DataFrame(columns=['category', 'text'])

for category in category_urls:
    print()
    print('#', category)
    print('# =============================================================================')

    # gets category page
    category_page = BeautifulSoup(requests.get(category_urls[category]).text, 'html.parser')

    # gets subsection of category page
    mw_pages = category_page.find('div', {'id':'mw-pages'})

    if mw_pages:
        mw_category = mw_pages.find('div', {'class':'mw-category'})
        if mw_category:
            for url in mw_category.find_all('a'):
                # gets page
                page = BeautifulSoup(requests.get(url['href']).text, 'html.parser')

                heading = page.find('h1', {'class':'firstHeading'})

                page_title = None
                if heading:
                    page_title = heading.text


                # gets content of page
                content = page.find('div', {'class':'mw-content-ltr'})

                if content:

                    # gets introduction of page
                    introduction = content.find('p')

                    if introduction is not None:
                        # extracts text
                        text = introduction.get_text()
                        dataframe = dataframe.append({'category' : category , 'text' : text}, ignore_index=True)

                        print('added..:', page_title)

dataframe.to_csv(datapath)
# =============================================================================